const fs = require('fs');
const path = require('path');
const getBestOfferCode = require('../index');
const { assert } = require('chai');

//import all required JSON files 
const custJson = JSON.parse(fs.readFileSync(path.join(__dirname, '../json/customerDetails.json'), 'utf-8'));
const offerJson = JSON.parse(fs.readFileSync(path.join(__dirname, '../json/availableOffers.json'), 'utf-8'));
const transJson = JSON.parse(fs.readFileSync(path.join(__dirname, '../json/transactionDetails.json'), 'utf-8'));

const expectedOutJson = {
    "requestID":"1",
    "codeType":"D",
    "validFor":"RC",
    "codeName":"KOTAKHOLY",
    "description":"ON FIRST CARD RELOAD CHARGES FREE FOR USD EUR GBP",
    "rateDetail":{
        "rateDiscountType":"F",
        "rateDiscountOn":"IBR",
        "rateDiscountOrMargin":"",
        "orgRate":"77.31",
        "rate":"75.81000",
        "ibr":"75.31",
        "cardRate":"77.31"
    },
    "chargeDetail":{
        "chargeDiscountType":"F",
        "chargesDiscount":"0",
        "orgCharge":"100",
        "charges":"118"
    },
    "transDetail":{
        "transTypeCode":
        "SVC-RELOAD",
        "currency":"USD",
        "fcyAmount":"1500",
        "lcyAmount":"113715.00",
        "totalAmount":"113833.00",
        "totalDiscount":"-2250.00"
    }
}

// describe('Discount code validation testing', function () {
//     const actualOutJson = JSON.parse(getBestOfferCode(custJson, offerJson, transJson));

//     it('Actual output should match with expected output', function () {
//         assert.deepEqual(actualOutJson, expectedOutJson);
//     });
// });