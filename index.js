const fs = require('fs');
const path = require("path");

const isCodeApplicable = require('./isCodeApplicable');
const calDiscount = require('./calculateRate');

function getBestOfferCode(custJson, offerJson, transJson){
    try{
        let bestDiscountDetail = {};
        let currentBestDiscount = 0;
        let isAnyCodeApplicable = 'N';

        for (let obj of offerJson.availableOfferCodes){
            if(!(obj.codeType == 'P')){
                let check_isCodeApplicable = JSON.parse(isCodeApplicable(custJson, obj, transJson));
                
                if(check_isCodeApplicable.applicable == 'Y'){
                    isAnyCodeApplicable = 'Y';
                    let discountForSelectedCode=JSON.parse(calDiscount(obj, transJson));
                    
                    let totalDiscount = Math.abs(parseFloat(discountForSelectedCode.offerdetail.transDetail.totalDiscount));
                    if(totalDiscount > currentBestDiscount){
                  
                        bestDiscountDetail = { 
                            "requestID" : transJson.requestID, 
                            "codeType" : obj.codeType,
                            "validFor" : obj.validFor,
                            "codeName" : obj.codeName,
                            "description" : obj.description,
                            "rateDetail" : discountForSelectedCode.offerdetail.rateDetail,
                            "chargeDetail" : discountForSelectedCode.offerdetail.chargeDetail,
                            "transDetail" : discountForSelectedCode.offerdetail.transDetail
                        };

                        currentBestDiscount = totalDiscount;
                    }
                }
            }
        }
        if(isAnyCodeApplicable == 'N' || (Object.keys(bestDiscountDetail).length === 0 && obj.constructor === Object)){
            return {"status":"No discount code is applicable"};
        }
        else{
            return JSON.stringify(bestDiscountDetail);
        }
    }
    catch (err) {
        return JSON.stringify({ "status": "unsuccess", "msg": err.message});
    }
}



//import all required JSON files 
const custJson = JSON.parse(fs.readFileSync(path.join(__dirname, './json/customerDetails.json'), 'utf-8'));
const offerJson = JSON.parse(fs.readFileSync(path.join(__dirname, './json/availableOffers.json'), 'utf-8'));
const transJson = JSON.parse(fs.readFileSync(path.join(__dirname, './json/transactionDetails.json'), 'utf-8'));

let output = getBestOfferCode(custJson, offerJson, transJson);
//console.log(output);

module.exports = getBestOfferCode;